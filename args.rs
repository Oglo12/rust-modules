#![allow(dead_code)]

pub struct EnvArgs {
    args: Vec<String>,
    flags: Vec<String>,
    raw: Vec<String>,
}

pub fn get_all() -> EnvArgs {
    let cmd_raw: Vec<String> = std::env::args().collect();

    let mut args: Vec<String> = Vec::new();
    let mut flags: Vec<String> = Vec::new();

    let mut record_flags = true;

    for i in cmd_raw.iter() {
        if i == "--" {
            record_flags = false;
        }

        if record_flags == true {
            if i.starts_with("--") {
                flags.push(i[2..].to_string());
            }

            else if i.starts_with("-") {
                let mut spot: usize = 0;

                for j in i.chars() {
                    if spot != 0 {
                        flags.push(j.to_string());
                    }

                    spot += 1;
                }
            }

            else {
                if i != "--" {
                    args.push(i.to_string());
                }
            }
        }

        else {
            if i != "--" {
                args.push(i.to_string());
            }
        }
    }
    
    let env_args = EnvArgs {
        args: args,
        flags: flags,
        raw: cmd_raw,
    };
    
    return env_args;
}

pub fn has_arg(arg: &str) -> bool {
    return get_args().contains(&arg.to_string());
}

pub fn has_flag(flag: &str) -> bool {
    return get_flags().contains(&flag.to_string());
}

pub fn has_raw(raw: &str) -> bool {
    return get_raw().contains(&raw.to_string());
}

pub fn get_args() -> Vec<String> {
    return get_all().args;
}

pub fn get_flags() -> Vec<String> {
    return get_all().flags;
}

pub fn get_raw() -> Vec<String> {
    return get_all().raw;
}
