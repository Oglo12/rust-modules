use std::io;
use std::io::Write;

pub fn input(text: &str, sep: bool) -> String {
    let mut prompt = String::new();
    if sep == true {
        print!("{} ", text);
    } else {
        print!("{}", text);
    }
    io::stdout().flush().unwrap();
    io::stdin()
        .read_line(&mut prompt)
        .expect("Failed to read line!");
    return prompt.trim().to_string();
}
