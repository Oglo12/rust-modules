use std::fs;
use std::fs::File;
use std::io::Write;

pub fn populate_read_to_string(file_name: &str, create_data: &str) -> String {
    match File::open(file_name) {
        Ok(_o) => {
            // Just do a normal return!
        }
        Err(_e) => {
            let mut file = File::create(file_name).expect("failed to create file");
            file.write_all(create_data.as_bytes())
                .expect("failed to write");
        }
    };

    let data = fs::read_to_string(file_name)
        .expect("failed to read file")
        .to_string();
    return data;
}
